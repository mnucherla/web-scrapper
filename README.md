## Sainsbury technical assesement 
The technical assessment is to create a standalone java application to scrap Sainsbury’s grocery site page and returns a JSON array of all the products on the page.
 
This is a Maven Java project using tech stack of Spring Boot (quick development) and Jsoup (web scrapping framework). 


Installation
--------------
* Clone this project
	``git clone https://mnucherla@bitbucket.org/mnucherla/web-scrapper.git``

* Build Application
	- ``mvn clean package`` (if maven installed)
	- ``mvnw clean package`` (alternatively)


Try it
--------------
Try one of the below

1. cd web-scrapper\target - ``java -jar web-scrapper-1.0.jar``
2. cd web-scrapper - ``mvnw spring-boot:run``