package com.sainsbury.web.scarpping.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import com.sainsbury.web.scarpping.mapper.CherriesDetailsPageMapper;
import com.sainsbury.web.scarpping.models.CherryProductInfo;

public class CherriesDetailsPageMapperTest {
	
	// Sainsbury product details page URL
	private String pdpPageUrlString = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--sweet-150g.html";
	
	// sample product detail page from Sainsbury live site
	private String sainsburyProductDetailsPage = "https://www.sainsburys.co.uk/shop/gb/groceries/sainsburys-blueberries-150g";
	
	// receipe page from Sainsbury live site
	private String dummyDetailsPage = "https://recipes.sainsburys.co.uk/recipes/10-minute-recipes/beetroot-soup-1";
	
	@Test
	public void testDetailsPageScrapping() {
		CherriesDetailsPageMapper mapper = new CherriesDetailsPageMapper();
		CherryProductInfo productInfo = mapper.map(pdpPageUrlString);
		assertNotNull(productInfo);
		assertEquals("Sainsbury's Blackberries, Sweet 150g",productInfo.getTitle());
		assertEquals("by Sainsbury's blackberries",productInfo.getDescription());
		assertEquals(32,productInfo.getCaloriesPer100G());
		assertEquals(1.5,productInfo.getUnitPrice());
	}
	
	@Test 
	public void testDetailsPageScrappingFromSainsuburyUrl() {
		CherriesDetailsPageMapper mapper = new CherriesDetailsPageMapper();
		CherryProductInfo productInfo = mapper.map(sainsburyProductDetailsPage);
		assertEquals("Sainsbury's Blueberries 150g",productInfo.getTitle());
		assertEquals("Blueberries",productInfo.getDescription());
		assertEquals(43,productInfo.getCaloriesPer100G());
		assertEquals(1.5,productInfo.getUnitPrice());
	}
	
	@Test 
	public void testDetailsPageScrappingForDummyUrl() {
		CherriesDetailsPageMapper mapper = new CherriesDetailsPageMapper();
		CherryProductInfo productInfo = mapper.map(dummyDetailsPage);
		assertNull(productInfo);
	}

}
