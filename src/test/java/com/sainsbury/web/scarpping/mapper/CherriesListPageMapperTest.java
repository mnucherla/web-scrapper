package com.sainsbury.web.scarpping.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.sainsbury.web.scarpping.WebScrapperApplication;

public class CherriesListPageMapperTest {
	// Sainsbury product details page URLs
	private String pdpPageUrl_1 = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--sweet-150g.html";
	private String pdpPageUrl_2 = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html";
	
	// list pages from Sainsubury site
	private String sainsburyPLPage = "https://www.sainsburys.co.uk/shop/gb/groceries/find-great-offers/fruit-and-veg";
	
	@Test
	public void testListPageUrlScrapping() {
		CherriesListPageMapper mapper = new CherriesListPageMapper();
		List<String> infoURLs = mapper.map(WebScrapperApplication.PLP_PAGE_URL);
		assertThat(infoURLs).hasSize(17);
		assertThat(infoURLs).contains(pdpPageUrl_1,pdpPageUrl_2);
	}
	
	@Test
	public void testListPageFromSainsuburyUrlScrapping() {
		CherriesListPageMapper mapper = new CherriesListPageMapper();
		List<String> infoURLs = mapper.map(sainsburyPLPage);
		assertThat(infoURLs).isEmpty();
		assertThat(infoURLs).hasSize(0);
	}

}
