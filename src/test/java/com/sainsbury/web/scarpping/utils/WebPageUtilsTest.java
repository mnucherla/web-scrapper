package com.sainsbury.web.scarpping.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.Test;

public class WebPageUtilsTest {

	@Test
	public void testValidWebPage() {
		Optional<Document> documentResult = WebPageUtils.of("http://google.com");
		assertTrue(documentResult.isPresent());
	}
	
	@Test
	public void testNonURL() {
		Optional<Document> documentResult = WebPageUtils.of("NOT a URL");
		assertFalse(documentResult.isPresent());
		documentResult = WebPageUtils.of("http://dummy-page.co.uk");
		assertFalse(documentResult.isPresent());
	}
	
	@Test
	public void testHrefUrl() {
		String html = "<html><head></head><body><div><a href=\"http://www.google.com\"></a></div></body></html>";
		Element element = Jsoup.parse(html).body();
		assertEquals("http://www.google.com",WebPageUtils.hrefString(element));
	}
	
	@Test
	public void testHrefAbsoluteUrl() {
		String html = "<html><head></head><body><div><a href=\"/google.html\"></a></div></body></html>";
		Document doc = Jsoup.parse(html);
		doc.setBaseUri("http://www.google.com");
		Element element = doc.body();
		assertEquals("http://www.google.com/google.html",WebPageUtils.hrefString(element));
	}
}
