package com.sainsbury.web.scarpping.models;

public class ProductTotal {
	
	private Double gross;
	
	private Double vat;

	public Double getGross() {
		return gross;
	}

	public void setGross(Double gross) {
		this.gross = gross;
		if(gross != null)
			setVat(gross * 0.2);
	}

	public Double getVat() {
		return vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}
	
	

}
