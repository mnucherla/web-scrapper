package com.sainsbury.web.scarpping.models;

public class CherryProductInfo {
	
	private String title;
	
	private String description;
	
	private Double unitPrice;
	
	private Integer caloriesPer100G;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getCaloriesPer100G() {
		return caloriesPer100G;
	}

	public void setCaloriesPer100G(Integer caloriesPer100G) {
		this.caloriesPer100G = caloriesPer100G;
	}

}
