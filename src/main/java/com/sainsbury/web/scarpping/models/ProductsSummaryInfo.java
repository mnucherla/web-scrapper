package com.sainsbury.web.scarpping.models;

import java.util.List;

public class ProductsSummaryInfo {
	
	private List<CherryProductInfo> results;
	
	private ProductTotal total;

	public List<CherryProductInfo> getResults() {
		return results;
	}

	public void setResults(List<CherryProductInfo> results) {
		this.results = results;
	}

	public ProductTotal getTotal() {
		return total;
	}

	public void setTotal(ProductTotal total) {
		this.total = total;
	}
	
}