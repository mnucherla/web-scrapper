package com.sainsbury.web.scarpping;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sainsbury.web.scarpping.mapper.CherriesDetailsPageMapper;
import com.sainsbury.web.scarpping.mapper.CherriesListPageMapper;
import com.sainsbury.web.scarpping.models.CherryProductInfo;
import com.sainsbury.web.scarpping.models.ProductTotal;
import com.sainsbury.web.scarpping.models.ProductsSummaryInfo;

@SpringBootApplication
public class WebScrapperApplication implements CommandLineRunner {
	
	private final static Logger logger = LoggerFactory.getLogger(WebScrapperApplication.class);
	
	// Product list page from assignment link
	public static final String PLP_PAGE_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html"; 
		

	public static void main(String[] args) {
		SpringApplication.run(WebScrapperApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		List<String> pdpUrls = new CherriesListPageMapper().map(PLP_PAGE_URL);
		if(! CollectionUtils.isEmpty(pdpUrls)) {
			CherriesDetailsPageMapper mapper = new CherriesDetailsPageMapper();
			List<CherryProductInfo> cherryProducts = 
					pdpUrls.stream()
						.map(mapper::map)
						.filter(productInfo -> productInfo != null)
						.collect(Collectors.toList());
			ProductsSummaryInfo productsSummaryInfo = new ProductsSummaryInfo();
			productsSummaryInfo.setResults(cherryProducts);
			ProductTotal totals = new ProductTotal();
			totals.setGross(cherryProducts.stream()
							.mapToDouble(CherryProductInfo::getUnitPrice)
							.sum());
			productsSummaryInfo.setTotal(totals);
			String output = new ObjectMapper()
					.setSerializationInclusion(Include.NON_NULL)
					.writerWithDefaultPrettyPrinter()
					.writeValueAsString(productsSummaryInfo);
			//logger.debug(output);
			System.out.println(output);
		}
		
	}

}
