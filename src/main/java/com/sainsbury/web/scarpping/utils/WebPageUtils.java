package com.sainsbury.web.scarpping.utils;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads a given URL's HTML representation  into Java Object structure.
 * Provides a convenient method to extract a table element into a list of objects
 */
public class WebPageUtils {
	
	private final static Logger logger = LoggerFactory.getLogger(WebPageUtils.class);
	
	public static Optional<Document> of(String webPageURL) {
		try {
			return Optional.of(Jsoup.connect(webPageURL).get());
		} catch (IOException   e) {
			logger.error("Unable to reach the website", e);
			return Optional.empty();
		} catch (IllegalArgumentException   e) {
			logger.error("Invalid website URL", e);
			return Optional.empty();
		}
	}
	
	public static String hrefString(Element containerEle) {
		if(Objects.nonNull(containerEle)) {
			Element anchorEle = containerEle.getElementsByTag("a").first();
			if(Objects.nonNull(anchorEle)) {
				return anchorEle.absUrl("href");
			}
		}
		return null;
	}

}
