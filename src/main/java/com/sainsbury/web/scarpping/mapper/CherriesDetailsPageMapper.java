package com.sainsbury.web.scarpping.mapper;

import static com.sainsbury.web.scarpping.mapper.utils.CherriesDetailsPageSelectionUtils.extractCaloriesFromInfoContainer;
import static com.sainsbury.web.scarpping.mapper.utils.CherriesDetailsPageSelectionUtils.extractDescriptionFromInfoContainer;
import static com.sainsbury.web.scarpping.mapper.utils.CherriesDetailsPageSelectionUtils.extractTitleFromSummaryContainer;
import static com.sainsbury.web.scarpping.mapper.utils.CherriesDetailsPageSelectionUtils.extractUnitPriceFromSummaryContainer;
import static java.util.Objects.nonNull;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.sainsbury.web.scarpping.models.CherryProductInfo;

public class CherriesDetailsPageMapper implements PageMapper<CherryProductInfo> {
	
	private static final String SUMMARY_CONTAINER_SELECTOR = "div.productSummary";
	
	private static final String INFO_CONTAINER_SELECTOR = "information";
	
	@Override
	public CherryProductInfo extractData(Document pdpPage) {
		
		Element summaryElement = pdpPage.selectFirst(SUMMARY_CONTAINER_SELECTOR);
		if(nonNull(summaryElement)) {
			String title = extractTitleFromSummaryContainer(summaryElement);
			Double unitPrice = extractUnitPriceFromSummaryContainer(summaryElement);
			if(StringUtils.isNotBlank(title) && unitPrice != null) {
				
				CherryProductInfo productInfo = new CherryProductInfo();
				productInfo.setTitle(title);
				productInfo.setUnitPrice(unitPrice);
			
				Element infoDiv = pdpPage.getElementById(INFO_CONTAINER_SELECTOR);
				productInfo.setDescription(extractDescriptionFromInfoContainer(infoDiv));
				productInfo.setCaloriesPer100G(extractCaloriesFromInfoContainer(infoDiv));
				return productInfo;
			}
		}
		return null;
	}

}
