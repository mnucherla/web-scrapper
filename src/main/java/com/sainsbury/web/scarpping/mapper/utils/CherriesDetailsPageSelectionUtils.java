package com.sainsbury.web.scarpping.mapper.utils;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.stripStart;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import java.util.Optional;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CherriesDetailsPageSelectionUtils {
	
	private final static Logger logger = LoggerFactory.getLogger(CherriesDetailsPageSelectionUtils.class);
	
	public static Integer extractCaloriesFromInfoContainer(Element infoDiv) {
		Element nutritionTableContainer = infoDiv.selectFirst(NUTRITION_TABLE_SELECTOR);
		if(nonNull(nutritionTableContainer)) {
			Element nutritionTableBody = nutritionTableContainer.getElementsByTag("tbody").first();
			if(nonNull(nutritionTableBody)) {
				//skip first row where energy is in kJs
				Optional<Element> secondRowEle = nutritionTableBody.getElementsByTag("tr")
													.stream().skip(1).findFirst();
				if(secondRowEle.isPresent()) {
					Elements secondRowCells = secondRowEle.get().getElementsByTag("td");
					Element caloriesEle = secondRowCells.isEmpty() ? null : secondRowCells.first();
					if(nonNull(caloriesEle) && caloriesEle.hasText()) {
						String caloriesText = substringBeforeLast(caloriesEle.text(), "kcal");
						if(isNotEmpty(caloriesText)) {
							try {
								return Integer.parseInt(caloriesText);
							} catch (NumberFormatException e) {
								logger.error("Invalid unit price", e);
								return null;
							}
						}
					}
				}
			}
		}
		return null;
	}

	public static String extractDescriptionFromInfoContainer(Element infoDiv) {
		Element descriptionContainer = infoDiv.selectFirst(DESCRIPTION_CONTAINER_SELECTOR);
		if(nonNull(descriptionContainer)) {
			Element descElement = descriptionContainer.getElementsByTag("p").first();
			// if the description is spread across multiple lines, then return only first line
			if(nonNull(descElement) && descElement.hasText()) {
				return descElement.text().lines().findFirst().get();
			}
		}
		return null;
	}

	public static Double extractUnitPriceFromSummaryContainer( Element summaryElement) {
		Element pricingContainer = summaryElement.selectFirst(PRICING_CONTAINER_SELECTOR);
		if(nonNull(pricingContainer)) {
			Element unitElement = pricingContainer.selectFirst(UNIT_CONTAINER_SELECTOR);
			Element unitClone = unitElement.clone();
			// removing child nodes
			unitClone.select("abbr").remove();
			if(unitClone.hasText()) {
				// remove currency symbol
				String unitText = stripStart(unitClone.text().trim(), "£");
				try {
					return Double.parseDouble(unitText);
				} catch (NullPointerException | NumberFormatException e) {
					logger.error("Invalid unit price", e);
					return null;
				}
			}
		}
		return null;
	}

	public static String extractTitleFromSummaryContainer(Element summaryElement) {
		Element titleContainer = summaryElement.selectFirst(TITLE_CONTAINER_SELECTOR);
		if(nonNull(titleContainer)) {
			Element unitElement = titleContainer.getElementsByTag("h1").first();
			return unitElement.text();
		}
		return null;
	}
	
	private static final String TITLE_CONTAINER_SELECTOR = "div.productTitleDescriptionContainer";
	private static final String PRICING_CONTAINER_SELECTOR = "div.pricing";
	private static final String UNIT_CONTAINER_SELECTOR = "p.pricePerUnit";
	private static final String DESCRIPTION_CONTAINER_SELECTOR = "div.productText";
	private static final String NUTRITION_TABLE_SELECTOR = "table.nutritionTable";

}
