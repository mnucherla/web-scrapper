package com.sainsbury.web.scarpping.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sainsbury.web.scarpping.utils.WebPageUtils;

public class CherriesListPageMapper implements PageMapper<List<String>>{
	
	private static final String PRODUCTS_GRID_VIEW_SELECTOR = "ul.productLister.gridView";
	
	private static final String PRODUCT_GRID_SELECTOR = "div.productNameAndPromotions";

	@Override
	public List<String> extractData(Document plpPage) {
		List<String> pdpPageUrls = new ArrayList<String>();
		Element gridViewElement = plpPage.selectFirst(PRODUCTS_GRID_VIEW_SELECTOR);
		if(Objects.nonNull(gridViewElement)) {
			Elements productListElements = gridViewElement.children();
			if(! productListElements.isEmpty()) {
				return productListElements.stream()
									.map(liEle -> liEle.selectFirst(PRODUCT_GRID_SELECTOR))
									.map(WebPageUtils::hrefString)
									.collect(Collectors.toList());
			}
		}
		return pdpPageUrls;
	}

}
