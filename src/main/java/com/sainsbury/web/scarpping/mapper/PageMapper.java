package com.sainsbury.web.scarpping.mapper;

import java.util.Optional;

import org.jsoup.nodes.Document;

import com.sainsbury.web.scarpping.utils.WebPageUtils;

public interface PageMapper<T> {
	
	default T map(String pageUrlString) {
		Optional<Document> pageResult = WebPageUtils.of(pageUrlString);
		if(pageResult.isEmpty()) return null;
		return extractData(pageResult.get());
	}

	T extractData(Document pageDocument);

}
